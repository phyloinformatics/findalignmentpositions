# READ-ME

## Overview

- Project's name: "indAlignmentPositions"
- License: [GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
- Description: "A [Python v3+](https://www.python.org/download/releases/3.0/) script that uses [Biopython](https://biopython.org/) to read data from a pairwise or multiple sequence alignment and return relative sequence positions together with some stats."

## Help

```
usage: findAlignmentPositions.py [-h] [-i INPUT] [-r REFERENCE] [-m MISSING]
                                 [-g GAP] [-s START] [-e END] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input pairwise or multiple sequence alignment in FASTA
                        format
  -r REFERENCE, --reference REFERENCE
                        ID (not description) of the reference sequence (use
                        the first if none is provided)
  -m MISSING, --missing MISSING
                        Symbol for missing data (default = N)
  -g GAP, --gap GAP     Symbol for gaps/ InDels (default = -)
  -s START, --start START
                        Start position in the alignment, counting from 0
                        (default = 0)
  -e END, --end END     End position in the alignment (default = last
                        position)
  -v, --verbose         Increases output verbosity
```

## Examples

### Files needed

1. `findAlignmentPositions.py` (script)
2. `example.aln.fasta` (multiple sequence alignment)

### Command line

The following command line was tested on Unix-bases (viz. MacOS and Linux) computers:

```Bash
findAlignmentPositions.py -i example.aln.fasta -v > example.stdout.txt 2> example.stderr.txt 
```

### Expected results

See the cpntents of the following files:

- `example.stdout.txt` (filtered FASTA alignment)
- `example.stderr.txt` (notes)

The program will print a filtered alignment to the standard output, considering the provided start and end positions of the alignment (default to start to finish). Positions that are absent from the selected reference sequence (i.e., gaps and missing data), will be ignored.

### How to include positions with gaps or missing data in the reference?

Use the argument options `-g` and `-m` to replace the gap and/or missing symbol by a character that is not present in the alignment (e.g., "#").

