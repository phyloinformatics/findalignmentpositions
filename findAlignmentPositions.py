#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# findAlignmentPositions.py

# A Python v3+ script that uses Biopython to read data from a pairwise or multiple sequence alignment and return relative sequence positions together with some stats.

##
# Import libraries
##

import argparse, re, sys
from Bio import SeqIO

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Input pairwise or multiple sequence alignment in FASTA format", type = str)
parser.add_argument("-r", "--reference", help = "ID (not description) of the reference sequence (use the first if none is provided)", type = str, required = False)
parser.add_argument("-m", "--missing", help = "Symbol for missing data (default = N)", type = str, required = False, default = "?")
parser.add_argument("-g", "--gap", help = "Symbol for gaps/ InDels (default = -)", type = str, required = False, default = "-")
parser.add_argument("-s", "--start", help = "Start position in the alignment, counting from 0 (default = 0)", type = int, required = False, default = 0)
parser.add_argument("-e", "--end", help = "End position in the alignment (default = last position)", type = int, required = False)
parser.add_argument("-v", "--verbose", help = "Increases output verbosity", action = "store_true")
args = parser.parse_args()

##
# Define functions
##

def out(t):
	return sys.stdout.write("{}\n".format(str(t).strip()))

def err(t):
	return sys.stderr.write("{}\n".format(str(t).strip()))

def gerRefId(filePath):
	if args.verbose: err("# No reference ID was provided. We will use the first one.")
	for record in SeqIO.parse(filePath, "fasta"):
		refid = record.id
		break
	return refid

def parseFasta(filePath, refid):
	if args.verbose: err("# Now we will parse the alignment data.")
	fastaDic = {}
	for record in SeqIO.parse(filePath, "fasta"):
		seqid = record.id
		seqnt = str(record.seq).upper()
		total = len(seqnt)
		fastaDic[seqid] = seqnt
		if args.verbose: err("# SeqID = {}, length = {}".format(seqid, total))
	return fastaDic

def getPositionsData(refid, refnt, query, seqnt, positionsDic):
	if args.verbose: err("# Now we are comparing {} to {}.".format(refid, query))
	gap = args.gap
	mis = args.missing
	total = len(refnt)
	for position in range(0, total):
		nt = refnt[position]
		if not nt in [gap, mis]:
			positionsDic[query][position] = seqnt[position]
	return positionsDic

def reportFilteredAlign(refid, refnt, positionsDic, map):
	gap = args.gap
	mis = args.missing
	total = len(refnt)
	start = args.start
	if not args.end:
		end = total
	else:
		end = args.end
	if args.verbose: err("# Now we will print the filtered alignment (from {} to {}; total = {}).".format(start, end, end - start + 1))
	out(">{}".format(refid))
	seq = ""
	for position in range(start, end):
		nt = refnt[position]
		if not nt in [gap, mis]:
			seq += nt
	out("{}".format(seq))
	for query in sorted(positionsDic):
		out(">{}".format(query))
		seq = ""
		for position in sorted(positionsDic[query]):
			if position >= start and position <= end:
				seq += positionsDic[query][position]
		out("{}".format(seq))
	return

##
# Execute functions
##

filePath = args.input

if not args.reference:
	refid = gerRefId(args.input)
else:
	refid = args.ref

if args.verbose: err("# RefID = {}\n# File = {}".format(refid, filePath))

fastaDic = parseFasta(filePath, refid)
refnt = fastaDic[refid]
del fastaDic[refid]
positionsDic = {}

for query in sorted(fastaDic):
	seqnt = fastaDic[query]
	positionsDic[query] = {}
	positionsDic = getPositionsData(refid, refnt, query, seqnt, positionsDic)

reportFilteredAlign(refid, refnt, positionsDic, map)

exit() # Quit this script
